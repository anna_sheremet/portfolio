window.onload = function() {
    function debounce(func, wait = 10, immediate = true) {
        let timeout;
        return function () {
            let context = this, args = arguments;
            let later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    let scrollPos = 0;
    const nav = document.querySelector('.site-nav');

    function checkPosition() {
        let windowY = window.scrollY;
        if (windowY < scrollPos) {
            // Scrolling UP
            nav.classList.add('is-visible');
            nav.classList.remove('is-hidden');
        } else {
            // Scrolling DOWN
            nav.classList.add('is-hidden');
            nav.classList.remove('is-visible');
        }
        scrollPos = windowY;
    }

    window.addEventListener('scroll', debounce(checkPosition));

    /*links to social media*/
  document.querySelectorAll('.dropdown-item').forEach(function(elem) {
    elem.addEventListener('click', function() {
      // Copy the element's text to the clipboard using clipboard-polyfill
      clipboard.writeText(elem.textContent);
    });
  });

    /*text*/

    /*opener*/
    function changeToggleButton() {
        let inner_text = document.getElementById('on');
        if (inner_text.innerHTML === "+") {
            inner_text.innerHTML = "-";
        } else {
            inner_text.innerHTML = "+";
        }
    }
}











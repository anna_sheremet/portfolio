from django.shortcuts import render
from portfolio.models import Project


def home(request):
    projects_start = Project.objects.all().order_by('-id')[:4]
    another_projects = Project.objects.all().order_by('-id')[4:]
    return render(request, 'home.html', {'projects_start': projects_start, 'another_projects': another_projects})

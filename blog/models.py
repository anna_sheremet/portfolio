from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=80)
    description = models.CharField(max_length=250)
    text = models.TextField(max_length=2000)
    image = models.ImageField(upload_to='blog/media/')
    url = models.URLField(blank=True)
    date = models.DateField()

from django.contrib import admin
from django.urls import path
from blog import views

app_name = 'blog'

urlpatterns = [
    path("", views.blog_page, name='blog'),
    path("<int:post_id>/", views.detail, name='detail'),
]
